/*
 * Copyright (C) 2016 huanghaibin_dev <huanghaibin_dev@163.com>
 * WebSite https://github.com/MiracleTimes-Dev
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.adapter.recyclerview;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * 基本的适配器
 */
public abstract class BaseRecyclerAdapter<T> extends BaseItemProvider {

    @SuppressWarnings("all")
    LayoutScatter mInflater;
    private List<T> mItems;
    Context mContext;

    public  BaseRecyclerAdapter(Context context,List<T> mItems) {
        mContext = context;
        this.mItems = new ArrayList<>();
        this.mItems.addAll(mItems);
        mInflater = LayoutScatter.getInstance(context);
    }



    @SuppressWarnings("ConstantConditions")
    public abstract ViewHolder onCreateViewHolder(ComponentContainer parent, int viewType);


    public abstract void onBindViewHolder(ViewHolder holder,int position);

    @Override
    public int getCount() {
        return mItems.size();
    }


    @SuppressWarnings("unused")
    void addAll(List<T> items) {
        if (items != null && items.size() > 0) {
            mItems.addAll(items);
            notifyDataSetItemRangeInserted(mItems.size(), items.size());
        }
    }

    void addItem(T item) {
        if (item != null) {
            this.mItems.add(item);
            notifyDataSetItemChanged(mItems.size());
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer parent) {
        int viewType = getItemComponentType(position);
        ViewHolder holder = onCreateViewHolder(parent, viewType);
        holder.setmPosition(position);
        onBindViewHolder(holder, position);
        return holder.itemView;
    }

    public static abstract class ViewHolder {

        public static final long NO_ID = -1;
        public static final int NO_POSITION = -1;
        int mPosition = NO_POSITION;
        long mItemId = NO_ID;

        public static Component itemView;
        private int type;
        public void setType(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }

        public ViewHolder(Component itemView) {
            if (itemView == null) {
                throw new IllegalArgumentException("itemComponent may not be null");
            }
            this.itemView = itemView;
        }

        public final int getAdapterPosition() {
            return mPosition;
        }

        public final long getItemId() {
            return mItemId;
        }

        public void setmPosition(int mPosition){
            this.mPosition = mPosition;
        }

    }
}
