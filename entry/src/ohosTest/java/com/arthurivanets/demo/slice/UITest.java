package com.arthurivanets.demo.slice;

import com.arthurivanets.bottomsheets.BottomSheet;
import com.arthurivanets.demo.MainAbility;
import com.arthurivanets.demo.ResourceTable;
import com.arthurivanets.demo.uitls.EventHelper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Button;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class UITest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }


    @Test
    public void showCustomBottomSheet() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice abilitySlice= (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Button button = (Button) abilitySlice.findComponentById(ResourceTable.Id_customBottomSheetBtn);
        sleep(1000);
        BottomSheet bottomSheet =abilitySlice.getBottomSheet();

        EventHelper.triggerClickEvent(mainAbility, button);
        sleep(2000);
        assertTrue("It's right", bottomSheet!=abilitySlice.getBottomSheet());
        sAbilityDelegator.stopAbility(mainAbility);
    }

    @Test
    public void showDeletionConfirmationBottomSheet() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice abilitySlice= (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Button button = (Button) abilitySlice.findComponentById(ResourceTable.Id_confirmationBottomSheetBtn);
        sleep(1000);
        BottomSheet bottomSheet =abilitySlice.getBottomSheet();
        EventHelper.triggerClickEvent(mainAbility, button);
        sleep(2000);
        assertTrue("It's right", bottomSheet!=abilitySlice.getBottomSheet());
        sAbilityDelegator.stopAbility(mainAbility);


    }

    @Test
    public void showActionsBottomSheet() {

        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice abilitySlice= (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Button button = (Button) abilitySlice.findComponentById(ResourceTable.Id_actionsBottomSheetBtn);
        sleep(1000);
        BottomSheet bottomSheet =abilitySlice.getBottomSheet();
        EventHelper.triggerClickEvent(mainAbility, button);
        sleep(2000);
        assertTrue("It's right", bottomSheet!=abilitySlice.getBottomSheet());
        sAbilityDelegator.stopAbility(mainAbility);


    }

    @Test
    public void showPeopleBottomSheet() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice abilitySlice= (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Button button = (Button) abilitySlice.findComponentById(ResourceTable.Id_customActionsBottomSheetBtn);
        sleep(1000);
        BottomSheet bottomSheet =abilitySlice.getBottomSheet();
        EventHelper.triggerClickEvent(mainAbility, button);
        sleep(2000);
        assertTrue("It's right", bottomSheet!=abilitySlice.getBottomSheet());
        sAbilityDelegator.stopAbility(mainAbility);
    }

    @Test
    public void showPeopleBottomSheet2() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice abilitySlice= (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Button button = (Button) abilitySlice.findComponentById(ResourceTable.Id_customActionsBottomSheetBtn);
        sleep(1000);
        BottomSheet bottomSheet =abilitySlice.getBottomSheet();
        EventHelper.triggerClickEvent(mainAbility, button);
        sleep(2000);
        assertTrue("It's right", bottomSheet!=abilitySlice.getBottomSheet());
        sAbilityDelegator.stopAbility(mainAbility);
    }

    @Test
    public void showActionsBottomSheet2() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1000);
        MainAbilitySlice abilitySlice= (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Button button = (Button) abilitySlice.findComponentById(ResourceTable.Id_customActionsBottomSheet2Btn);
        sleep(1000);
        BottomSheet bottomSheet =abilitySlice.getBottomSheet();
        EventHelper.triggerClickEvent(mainAbility, button);
        sleep(2000);
        assertTrue("It's right", bottomSheet!=abilitySlice.getBottomSheet());
        sAbilityDelegator.stopAbility(mainAbility);

    }


    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}