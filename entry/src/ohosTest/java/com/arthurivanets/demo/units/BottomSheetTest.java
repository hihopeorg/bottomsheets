package com.arthurivanets.demo.units;

import com.arthurivanets.bottomsheets.BottomSheet;
import com.arthurivanets.bottomsheets.sheets.config.ActionPickerConfig;
import com.arthurivanets.bottomsheets.sheets.model.Option;
import com.arthurivanets.bottomsheets.sheets2.ActionsKt;
import com.arthurivanets.bottomsheets.sheets2.adapter.AdapterCommonsKt;
import com.arthurivanets.bottomsheets.sheets2.dsl.ActionPickerDslBuilderKt;
import com.arthurivanets.bottomsheets.sheets2.dsl.BaseActionPickerDslBuilderKt;
import com.arthurivanets.bottomsheets.sheets2.dsl.CustomActionPickerDslBuilder;
import com.arthurivanets.bottomsheets.sheets2.dsl.DslCommonKt;
import com.arthurivanets.bottomsheets.sheets2.extensions.ContextExtensionsKt;
import com.arthurivanets.bottomsheets.util.BingBottomSheetContainer;
import com.arthurivanets.bottomsheets.util.MathUtils;
import com.arthurivanets.bottomsheets.util.Preconditions;
import com.arthurivanets.demo.ResourceTable;
import com.arthurivanets.demo.adapters.persons.PersonItem2;
import com.arthurivanets.demo.model.Person;
import com.arthurivanets.demo.model.PersonAction;
import com.arthurivanets.demo.util.providers.ActionsProvider;
import com.arthurivanets.demo.util.providers.ConfirmationActionsProvider;
import com.arthurivanets.demo.util.providers.PersonProvider;
import com.arthurivanets.demo.util.providers.ProviderUtils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.element.Element;
import ohos.app.Context;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class BottomSheetTest {

    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private Context mContext;

    @Before
    public void setUp() throws Exception {
        mContext = sAbilityDelegator.getAppContext();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getGeneralDeletionConfirmationActionOptions() {
        List<Option> options = ConfirmationActionsProvider.getGeneralDeletionConfirmationActionOptions(mContext);
        Assert.assertTrue("options is null or error", options.size() > 0);

    }

    @Test
    public void getActionOptions() {
        List<Option> options = ActionsProvider.getActionOptions(mContext);
        Assert.assertTrue("options is null or error", options.size() > 0);
    }


    @Test
    public void getPeople() {
        List<Person> personIterator = PersonProvider.getPeople();
        Assert.assertTrue("personIterator is null or error", personIterator.size() > 0);
    }

    @Test
    public void clamp() {
        int value = MathUtils.clamp(10, 9, 11);
        Assert.assertTrue("return to value is max", value ==10);
    }

    @Test
    public void checkNonNull() {
        String title = "name";
        String object = Preconditions.checkNonNull(title);
        Assert.assertTrue("object is null or error", !object.isEmpty());
    }

    @Test
    public void action() {
        ActionPickerDslBuilderKt.ActionPickerConfigBuilder actionPickerConfigBuilder = new ActionPickerDslBuilderKt.ActionPickerConfigBuilder(mContext);
        actionPickerConfigBuilder.action(new ActionsKt.ActionId(1), ResourceTable.Graphic_ic_outline_local_printshop_24px, ResourceTable.String_action_print);
        BaseActionPickerDslBuilderKt.ActionPickerConfig pickerConfig = actionPickerConfigBuilder.build();
        List<AdapterCommonsKt.Item> items = pickerConfig.getItems();
        Assert.assertTrue("items is null or error", items.size() > 0);
    }

    @Test
    public void item() {
        CustomActionPickerDslBuilder.CustomActionPickerConfigBuilder actionPickerConfigBuilder = new CustomActionPickerDslBuilder.CustomActionPickerConfigBuilder(mContext);
        List<Person> personIterator = PersonProvider.getPeople();
        PersonAction personAction = new PersonAction(personIterator.get(0));
        PersonItem2 personItem2 = new PersonItem2(personAction);
        actionPickerConfigBuilder.item(personItem2);
        BaseActionPickerDslBuilderKt.ActionPickerConfig actionPickerConfig = actionPickerConfigBuilder.build();
        List<AdapterCommonsKt.Item> actionPickerConfigItems = actionPickerConfig.getItems();
        Assert.assertNotNull("actionPickerConfigItems is null or error", actionPickerConfigItems.size() > 0);

    }



    @Test
    public void build() {
        com.arthurivanets.bottomsheets.sheets.config.Config.Builder builder = new com.arthurivanets.bottomsheets.sheets.config.Config.Builder(mContext);
        ActionPickerConfig actionPickerConfig = builder.build();
        float sheetCornerRadius = actionPickerConfig.getSheetCornerRadius();
        Assert.assertTrue("sheetCornerRadius is null or error", sheetCornerRadius > 0);
    }


    @Test
    public void getIcon() {
        DslCommonKt.Icon icon = new DslCommonKt.Icon(null, ResourceTable.Graphic_ic_delete_black_24dp);
        Element element = icon.getIcon(mContext);
        Assert.assertNotNull("element is null or error", element);
    }


    @Test
    public void createOption() {
        Option option = ProviderUtils.createOption(mContext,
                1l,
                ResourceTable.Graphic_ic_outline_local_printshop_24px,
                mContext.getString(ResourceTable.String_action_print));
        Assert.assertNotNull("option is null or error", option);
    }

    @Test
    public void getComponentContainer() {
        BingBottomSheetContainer bingBottomSheetContainer = BingBottomSheetContainer.getInstance();
        bingBottomSheetContainer.initBingBottomSheetContainer(new DependentLayout(mContext));
        Component component = bingBottomSheetContainer.getComponentContainer();
        Assert.assertNotNull("component is null or error", component);
    }


    @Test
    public void getDimension() {
        float dimension = ContextExtensionsKt.getDimension(mContext, ResourceTable.Float_action_picker_bottom_sheet_divider_size);
        Assert.assertNotNull("dimension is null or error", dimension > 0);
    }

    @Test
    public void getCompatColor() {
        float compatColor = ContextExtensionsKt.getCompatColor(mContext, ResourceTable.Color_action_item_icon);
        Assert.assertNotNull("compatColor is null or error", compatColor > 0);

    }


}
