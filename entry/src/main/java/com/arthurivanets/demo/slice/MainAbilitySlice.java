/*
 * Copyright 2017 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arthurivanets.demo.slice;

import com.arthurivanets.bottomsheets.BottomSheet;
import com.arthurivanets.bottomsheets.ktx.ConfigExtensionsKt;
import com.arthurivanets.bottomsheets.sheets.adapters.actionpicker.BaseActionItem;
import com.arthurivanets.bottomsheets.sheets.config.ActionPickerConfig;
import com.arthurivanets.bottomsheets.sheets.listeners.OnItemSelectedListener;
import com.arthurivanets.bottomsheets.sheets.model.Option;
import com.arthurivanets.bottomsheets.sheets2.ActionsKt;
import com.arthurivanets.bottomsheets.sheets2.dsl.ActionPickerDslBuilderKt;
import com.arthurivanets.bottomsheets.sheets2.dsl.CustomActionPickerDslBuilder;
import com.arthurivanets.bottomsheets.util.BingBottomSheetContainer;
import com.arthurivanets.demo.ResourceTable;
import com.arthurivanets.demo.adapters.persons.PersonItem;
import com.arthurivanets.demo.adapters.persons.PersonItem2;
import com.arthurivanets.demo.model.Person;
import com.arthurivanets.demo.model.PersonAction;
import com.arthurivanets.demo.ui.SimpleCustomBottomSheet;
import com.arthurivanets.demo.util.providers.ActionsProvider;
import com.arthurivanets.demo.util.providers.ConfirmationActionsProvider;
import com.arthurivanets.demo.util.providers.PersonProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.List;

import static com.arthurivanets.bottomsheets.ktx.BottomSheetsExtensionKt.showActionPickerBottomSheet;
import static com.arthurivanets.bottomsheets.ktx.BottomSheetsExtensionKt.showCustomActionPickerBottomSheet;


public class MainAbilitySlice extends AbilitySlice {
    private BottomSheet bottomSheet = null;

    public BottomSheet getBottomSheet() {
        return bottomSheet;
    }


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        ComponentContainer componentContainer = (ComponentContainer) findComponentById(ResourceTable.Id_content);
        BingBottomSheetContainer.getInstance().initBingBottomSheetContainer(componentContainer);
        initView();


    }

    private void initView() {
        findComponentById(ResourceTable.Id_customBottomSheetBtn).setClickedListener(component -> {
            if (bottomSheet == null || bottomSheet.getState().equals(BottomSheet.State.COLLAPSED)) {
                showCustomBottomSheet();
            }

        });

        findComponentById(ResourceTable.Id_confirmationBottomSheetBtn).setClickedListener(component -> {
            if (bottomSheet == null || bottomSheet.getState().equals(BottomSheet.State.COLLAPSED)) {
                showDeletionConfirmationBottomSheet();
            }
        });

        findComponentById(ResourceTable.Id_actionsBottomSheetBtn).setClickedListener(component -> {
            if (bottomSheet == null || bottomSheet.getState().equals(BottomSheet.State.COLLAPSED)) {
                showActionsBottomSheet();
            }
        });
        findComponentById(ResourceTable.Id_customActionsBottomSheetBtn).setClickedListener(component -> {
            if (bottomSheet == null || bottomSheet.getState().equals(BottomSheet.State.COLLAPSED)) {
                showPeopleBottomSheet();
            }
        });
        findComponentById(ResourceTable.Id_actionsBottomSheet2Btn).setClickedListener(component -> {
            if (bottomSheet == null || bottomSheet.getState().equals(BottomSheet.State.COLLAPSED)) {
                showActionsBottomSheet2();
            }
        });
        findComponentById(ResourceTable.Id_customActionsBottomSheet2Btn).setClickedListener(component -> {
            if (bottomSheet == null || bottomSheet.getState().equals(BottomSheet.State.COLLAPSED)) {
                showPeopleBottomSheet2();
            }
        });
    }


    private void showPeopleBottomSheet() {
        dismissBottomSheet();

        ArrayList<PersonItem> items = new ArrayList<>();
        for (Person person : PersonProvider.getPeople()) {
            items.add(new PersonItem(person));
        }
        com.arthurivanets.bottomsheets.sheets.config.Config.Builder builder = new com.arthurivanets.bottomsheets.sheets.config.
                Config.Builder(this);
        ActionPickerConfig actionPickerConfig = builder.build();
        bottomSheet = showCustomActionPickerBottomSheet(this.getAbility(), items, actionPickerConfig, new OnItemSelectedListener() {
            @Override
            public void onItemSelected(Object item) {
                if (item instanceof BaseActionItem) {
                    BaseActionItem baseActionItem = (BaseActionItem) item;
                    shortToast(baseActionItem.getItemModel().toString());
                }

            }
        }, null);
    }


    private void showPeopleBottomSheet2() {
        List<Person> personIterator = PersonProvider.getPeople();

        CustomActionPickerDslBuilder.CustomActionPickerConfigBuilder actionPickerConfigBuilder = new CustomActionPickerDslBuilder.CustomActionPickerConfigBuilder(this);
        for (Person person : personIterator) {
            PersonAction personAction = new PersonAction(person);
            PersonItem2 personItem2 = new PersonItem2(personAction);
            actionPickerConfigBuilder.item(personItem2);

        }

        actionPickerConfigBuilder.onItemClickListener = action -> {
            shortToast("Person Clicked: [id: " + action.id + "]");
        };

        bottomSheet = CustomActionPickerDslBuilder.customActionPickerBottomSheet(this.getAbility(), actionPickerConfigBuilder);
        bottomSheet.show();
    }


    private void showActionsBottomSheet2() {
        ActionPickerDslBuilderKt.ActionPickerConfigBuilder actionPickerConfigBuilder = new ActionPickerDslBuilderKt.ActionPickerConfigBuilder(this.getContext());
        actionPickerConfigBuilder.action(new ActionsKt.ActionId(1), ResourceTable.Graphic_ic_outline_local_printshop_24px, ResourceTable.String_action_print);
        actionPickerConfigBuilder.action(new ActionsKt.ActionId(2), ResourceTable.Graphic_ic_outline_save_24px, ResourceTable.String_action_save);
        actionPickerConfigBuilder.action(new ActionsKt.ActionId(3), ResourceTable.Graphic_ic_outline_offline_bolt_24px, ResourceTable.String_action_recharge);
        actionPickerConfigBuilder.action(new ActionsKt.ActionId(4), ResourceTable.Graphic_ic_outline_thumb_up_24px, ResourceTable.String_action_like);
        actionPickerConfigBuilder.action(new ActionsKt.ActionId(5), ResourceTable.Graphic_ic_outline_thumb_down_24px, ResourceTable.String_action_dislike);

        actionPickerConfigBuilder.onItemClickListener = action -> {
            shortToast("ActionID: " + action.id);
        };
        bottomSheet = ActionPickerDslBuilderKt.actionPickerBottomSheet(this.getAbility(), actionPickerConfigBuilder);
        bottomSheet.show();
    }

    private void showCustomBottomSheet() {
        dismissBottomSheet();
        bottomSheet = new SimpleCustomBottomSheet(this.getAbility(), new com.arthurivanets.bottomsheets.config.Config.Builder(this).build());
        bottomSheet.show();
    }


    private void showDeletionConfirmationBottomSheet() {
        dismissBottomSheet();
        String title = getString(ResourceTable.String_confirmation_title_item_deletion);
        List<Option> options = ConfirmationActionsProvider.getGeneralDeletionConfirmationActionOptions(this);

        ActionPickerConfig actionPickerConfig = ConfigExtensionsKt.actionPickerConfig(this, title);

        bottomSheet = showActionPickerBottomSheet(this.getAbility(), options, actionPickerConfig, new OnItemSelectedListener<Option>() {
            @Override
            public void onItemSelected(Option item) {
                shortToast(item.getTitle().toString());
            }
        }, null);

    }

    private void showActionsBottomSheet() {
        dismissBottomSheet();
        List<Option> options = ActionsProvider.getActionOptions(this.getContext());
        com.arthurivanets.bottomsheets.sheets.config.Config.Builder builder = new com.arthurivanets.bottomsheets.sheets.config.Config.Builder(this);
        ActionPickerConfig actionPickerConfig = builder.build();
        bottomSheet = showActionPickerBottomSheet(this.getAbility(), options, actionPickerConfig, new OnItemSelectedListener<Option>() {
            @Override
            public void onItemSelected(Option item) {
                shortToast(item.getTitle().toString());
            }
        }, null);

    }

    private void dismissBottomSheet() {
        dismissBottomSheet(true);
    }

    private void dismissBottomSheet(boolean animate) {
        if (bottomSheet != null) {
            bottomSheet.dismiss(animate);
        }
        bottomSheet = null;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void shortToast(String str) {
        ToastDialog toastDialog = new ToastDialog(this);
        toastDialog.setText(str);
        toastDialog.show();

    }


}
