/*
 * Copyright 2017 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.demo.ui;

import com.arthurivanets.bottomsheets.BaseBottomSheet;
import com.arthurivanets.bottomsheets.config.BaseConfig;
import com.arthurivanets.demo.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

public class SimpleCustomBottomSheet extends BaseBottomSheet {


    public SimpleCustomBottomSheet(Ability hostActivity, BaseConfig config) {
        super(hostActivity, config);
    }

    @Override
    protected Component onCreateSheetContentView(Context context) {
        return LayoutScatter.getInstance(context).parse(
                ResourceTable.Layout_view_simple_custom_bottom_sheet,
                this,
                false
        );
    }
}
