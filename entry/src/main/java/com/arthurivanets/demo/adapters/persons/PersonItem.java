/*
 * Copyright 2017 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arthurivanets.demo.adapters.persons;

import com.arthurivanets.adapster.ktx.ListenerExtensionsKt;
import com.arthurivanets.adapter.Adapter;
import com.arthurivanets.adapter.listeners.OnItemClickListener;
import com.arthurivanets.adapter.model.Item;
import com.arthurivanets.adapter.model.markers.Trackable;
import com.arthurivanets.bottomsheets.sheets.adapters.actionpicker.ActionPickerItemResources;
import com.arthurivanets.bottomsheets.sheets.adapters.actionpicker.BaseActionItem;
import com.arthurivanets.demo.ResourceTable;
import com.arthurivanets.demo.model.Person;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

public class PersonItem extends BaseActionItem<Person, PersonItemViewHolder, ActionPickerItemResources> implements Trackable<Long> {


    public PersonItem(Person itemModel) {
        super(itemModel);
    }

    @Override
    public void setOnItemClickListener(PersonItemViewHolder viewHolder, OnItemClickListener<BaseActionItem<Person, PersonItemViewHolder, ActionPickerItemResources>> onItemClickListener) {
        ListenerExtensionsKt.setOnItemClickListener(viewHolder.itemView, this, 0, onItemClickListener);

    }

    @Override
    public PersonItemViewHolder init(Adapter<? extends Item> adapter, ComponentContainer parent, LayoutScatter inflater, ActionPickerItemResources resources) {
        return new PersonItemViewHolder(
                inflater.parse(
                        getLayout(),
                        parent,
                        false
                )
        );
    }

    @Override
    public int getLayout() {
        return ResourceTable.Layout_item_person;
    }

    @Override
    public Long getTrackKey() {
        return getItemModel().getId();
    }
}
