/*
 * Copyright 2017 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arthurivanets.demo.adapters.persons;

import com.arthurivanets.adapter.model.BaseItem;
import com.arthurivanets.demo.ResourceTable;
import com.arthurivanets.demo.model.Person;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.ElementScatter;


public class PersonItemViewHolder extends BaseItem.ViewHolder<Person> {
    public Image imageIv;
    public Text fullNameTv;
    public Text usernameTv;

    public PersonItemViewHolder(Component itemView) {
        super(itemView);
        imageIv = (Image) itemView.findComponentById(ResourceTable.Id_imageIv);
        fullNameTv = (Text) itemView.findComponentById(ResourceTable.Id_fullNameTv);
        usernameTv = (Text) itemView.findComponentById(ResourceTable.Id_usernameTv);
    }

    @Override
    public void bindData(Person person) {
        super.bindData(person);
        handleData(person);
    }


    private void handleData(Person data) {
        fullNameTv.setText(data.getFullName());
        usernameTv.setText(usernameTv.getContext().getString(ResourceTable.String_person_item_username_template, data.getUsername()));
        loadImage(data.getImageResourceId());
    }

    private void loadImage(int imageResourceId) {
        imageIv.setBackground(ElementScatter.getInstance(imageIv.getContext()).parse(imageResourceId));
    }

}
