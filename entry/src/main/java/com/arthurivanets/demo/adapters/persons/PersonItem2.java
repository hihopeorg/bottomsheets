/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.demo.adapters.persons;

import com.arthurivanets.adapter.recyclerview.BaseRecyclerAdapter;
import com.arthurivanets.bottomsheets.sheets2.adapter.AdapterCommonsKt;
import com.arthurivanets.demo.ResourceTable;
import com.arthurivanets.demo.model.Person;
import com.arthurivanets.demo.model.PersonAction;
import ohos.agp.components.*;
import ohos.agp.components.element.ElementScatter;

public class PersonItem2 extends AdapterCommonsKt.AbstractItem<PersonAction, PersonItem2.ViewHolder> {


    public PersonItem2(PersonAction personAction) {
        super(personAction);
    }

    @Override
    public BaseRecyclerAdapter.ViewHolder create(LayoutScatter inflater, ComponentContainer parent) {
        return new ViewHolder(
                inflater.parse(
                        ResourceTable.Layout_item_person,
                        parent,
                        false
                )
        );
    }

    @Override
    protected void performBinding(ViewHolder viewHolder) {
        viewHolder.bindData(model.getPerson());
    }

    static class ViewHolder extends BaseRecyclerAdapter.ViewHolder implements AdapterCommonsKt.HasListeners {

        private Image imageIv;
        private Text fullNameTv;
        private Text usernameTv;


        public ViewHolder(Component itemView) {
            super(itemView);
            imageIv = (Image) itemView.findComponentById(ResourceTable.Id_imageIv);
            fullNameTv = (Text) itemView.findComponentById(ResourceTable.Id_fullNameTv);
            usernameTv = (Text) itemView.findComponentById(ResourceTable.Id_usernameTv);
        }

        public void bindData(Person person) {
            fullNameTv.setText(person.getFullName());
            usernameTv.setText(usernameTv.getContext().getString(ResourceTable.String_person_item_username_template, person.getUsername()));
            loadImage(person.getImageResourceId());
        }

        private void loadImage(int imageResourceId) {
            imageIv.setBackground(ElementScatter.getInstance(imageIv.getContext()).parse(imageResourceId));
        }

    }


}
