/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.demo.util.providers;

import com.arthurivanets.bottomsheets.sheets.model.Option;
import com.arthurivanets.demo.ResourceTable;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

import static com.arthurivanets.demo.util.providers.ProviderUtils.createOption;

public class ActionsProvider {
    public final static long PRINT = 1L;
    public final static long SAVE = 2L;
    public final static long RECHARGE = 3L;
    public final static long LIKE = 4L;
    public final static long DISLIKE = 5L;

    public static List<Option> getActionOptions(Context context) {
        ArrayList<Option> arrayList = new ArrayList<Option>();
// Printing Option
        arrayList.add(createOption(context,
                PRINT,
                ResourceTable.Graphic_ic_outline_local_printshop_24px,
                context.getString(ResourceTable.String_action_print)
        ));
        // Saving Option
        arrayList.add(createOption(context,
                SAVE,
                ResourceTable.Graphic_ic_outline_save_24px,
                context.getString(ResourceTable.String_action_save)
        ));
        // Recharging Option
        arrayList.add(createOption(context,
                RECHARGE,
                ResourceTable.Graphic_ic_outline_offline_bolt_24px,
                context.getString(ResourceTable.String_action_recharge)
        ));

        //            // Liking Option
        arrayList.add(createOption(context,
                LIKE,
                ResourceTable.Graphic_ic_outline_thumb_up_24px,
                context.getString(ResourceTable.String_action_like)
        ));
        // Disliking Option
        arrayList.add(createOption(context,
                DISLIKE,
                ResourceTable.Graphic_ic_outline_thumb_down_24px,
                context.getString(ResourceTable.String_action_dislike)
        ));
        return arrayList;
    }
}
