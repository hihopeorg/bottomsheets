/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.demo.util.providers;

import com.arthurivanets.adapter.annotation.DrawableRes;
import com.arthurivanets.bottomsheets.sheets.model.Option;
import com.arthurivanets.demo.ResourceTable;
import ohos.app.Context;

public class ProviderUtils {

    public static Option createOption(
            Context context, long id,
            @DrawableRes
                    int iconId,
            CharSequence title) {
        return new Option()
                .setId(id)
                .setIconId(iconId)
                .setIconColor(context.getColor(ResourceTable.Color_colorSecondaryTextGray))
                .setTitle(title)
                .setTitleColor(context.getColor(ResourceTable.Color_colorPrimaryTextBlack));
    }
}
