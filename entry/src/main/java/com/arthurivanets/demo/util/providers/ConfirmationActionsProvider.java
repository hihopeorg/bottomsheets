/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.demo.util.providers;

import com.arthurivanets.bottomsheets.sheets.model.Option;
import com.arthurivanets.demo.ResourceTable;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

import static com.arthurivanets.demo.util.providers.ProviderUtils.createOption;

public class ConfirmationActionsProvider {
    public final static long CONFIRM = 1L;
    public final static long CANCEL = 2L;
    public final static long OTHER = 3L;


    public static List<Option> getGeneralDeletionConfirmationActionOptions(Context context) {
        ArrayList<Option> arrayList = new ArrayList<>();
        arrayList.add(createOption(context,
                CONFIRM,
                ResourceTable.Graphic_ic_outline_delete_outline_24px,
                context.getString(ResourceTable.String_action_delete)));
        arrayList.add(createOption(context,
                CANCEL,
                ResourceTable.Graphic_ic_outline_cancel_24px,
                context.getString(ResourceTable.String_action_cancel)
        ));

        return arrayList;

    }

}
