/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.demo.util.providers;

import com.arthurivanets.demo.ResourceTable;
import com.arthurivanets.demo.model.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonProvider {
    public static List<Person> getPeople() {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person(1l, "danicajohns", "Danica Johns", ResourceTable.Graphic_ic_woman_1));
        personList.add(new Person(2l, "harryaguilar", "Harry Aguilar", ResourceTable.Graphic_ic_man_2));
        personList.add(new Person(3l, "houstonroth", "Houston Roth", ResourceTable.Graphic_ic_man_3));
        personList.add(new Person(4l, "george41", "George Evans", ResourceTable.Graphic_ic_man_4));
        return personList;
    }
}
