package com.arthurivanets.demo.model;

import com.arthurivanets.bottomsheets.sheets2.ActionsKt;

public class PersonAction extends ActionsKt.Action {
    private Person person;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public PersonAction(Person person) {
        this.person=person;
        id = person.getId();
    }
}
