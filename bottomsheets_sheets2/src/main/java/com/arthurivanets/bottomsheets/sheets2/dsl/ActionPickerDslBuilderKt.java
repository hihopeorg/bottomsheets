/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.sheets2.dsl;

import com.arthurivanets.adapter.annotation.DrawableRes;
import com.arthurivanets.adapter.annotation.StringRes;
import com.arthurivanets.adapter.recyclerview.BaseRecyclerAdapter;
import com.arthurivanets.bottomsheets.BottomSheet;
import com.arthurivanets.bottomsheets.sheets2.ActionPickerBottomSheet;
import com.arthurivanets.bottomsheets.sheets2.ActionsKt;
import com.arthurivanets.bottomsheets.sheets2.ResourceTable;
import com.arthurivanets.bottomsheets.sheets2.adapter.AdapterCommonsKt;
import com.arthurivanets.bottomsheets.sheets2.adapter.DividerItem;
import com.arthurivanets.bottomsheets.sheets2.adapter.SimpleActionItem;
import com.arthurivanets.bottomsheets.sheets2.kotlin.Unit;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.agp.components.element.Element;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.util.ArrayList;

public class ActionPickerDslBuilderKt {

    public static BottomSheet actionPickerBottomSheet(Fraction fraction, ActionPickerConfigBuilder actionPickerConfigBuilder) {
        return actionPickerBottomSheet(fraction.getFractionAbility(), actionPickerConfigBuilder);
    }

    public static BottomSheet actionPickerBottomSheet(Ability ability, ActionPickerDslBuilderKt.ActionPickerConfigBuilder builder) {
        BaseActionPickerDslBuilderKt.ActionPickerConfig pickerConfig = builder.build();
        ActionPickerBottomSheet actionPickerBottomSheet = new ActionPickerBottomSheet(ability, pickerConfig);
        return actionPickerBottomSheet;
    }


    public static class ActionPickerConfigBuilder extends BaseActionPickerDslBuilderKt.AbstractActionPickerConfigBuilder {
        private ItemTheme itemTheme;
        private Context context;

        public ActionPickerConfigBuilder(Context context) {
            super(context);
            itemTheme = new ItemTheme.Builder(context).build();
            this.context = context;

        }


        public void itemTheme(ItemTheme.Builder builder) {
            itemTheme = new ItemTheme.Builder(context).build();
        }


        public ActionPickerConfigBuilder section() {
            items.add(new DividerItem(new Unit()));
            return this;
        }

        public void action(ActionsKt.ActionId id, @StringRes int titleId, ArrayList args) {
            ActionsKt.SimpleAction simpleAction = new ActionsKt.SimpleAction();
            DslCommonKt.Text text = new DslCommonKt.Text();
            text.setArgs(args);
            text.setResourceId(titleId);
            simpleAction.title = text;
            simpleAction.id = id.id;
            items.add(new SimpleActionItem(simpleAction));

        }

        public void action(ActionsKt.ActionId id, @DrawableRes int iconId, @StringRes int titleId, Object... args) {
            ActionsKt.SimpleAction simpleAction = new ActionsKt.SimpleAction();
            simpleAction.id = id.id;
            simpleAction.icon = new DslCommonKt.Icon(null, iconId);
            DslCommonKt.Text text = new DslCommonKt.Text();
            text.setArgs(args);
            text.setResourceId(titleId);
            simpleAction.title = text;
            items.add(new SimpleActionItem(simpleAction));
        }


        public void action(ActionsKt.ActionId id, Element icon, @StringRes int titleId, ArrayList args) {
            ActionsKt.SimpleAction simpleAction = new ActionsKt.SimpleAction();
            simpleAction.id = id.id;
            simpleAction.icon = new DslCommonKt.Icon(icon, 0);
            DslCommonKt.Text text = new DslCommonKt.Text();
            text.setArgs(args);
            text.setResourceId(titleId);
            simpleAction.title = text;
            items.add(new SimpleActionItem(simpleAction));
        }

        public void action(ActionsKt.ActionId id, @DrawableRes int iconId, CharSequence title) {
            ActionsKt.SimpleAction simpleAction = new ActionsKt.SimpleAction();
            simpleAction.id = id.id;
            simpleAction.icon = new DslCommonKt.Icon(null, iconId);
            DslCommonKt.Text text = new DslCommonKt.Text();
            text.setText(title);
            simpleAction.title = text;
            items.add(new SimpleActionItem(simpleAction));

        }


        public void action(ActionsKt.ActionId id, Element icon, CharSequence title) {
            ActionsKt.SimpleAction simpleAction = new ActionsKt.SimpleAction();
            simpleAction.id = id.id;
            if (icon != null) {
                simpleAction.icon = new DslCommonKt.Icon(icon, 0);
            }
            DslCommonKt.Text text = new DslCommonKt.Text();
            text.setText(title);
            simpleAction.title = text;
            items.add(new SimpleActionItem(simpleAction));

        }


        @Override
        protected AdapterCommonsKt.ItemThemeApplier createItemThemeApplier() {
            return new DefaultItemThemeApplier(dividerColor, itemTheme);
        }


    }


    public static class ItemTheme {
        public int iconColor;
        public int textColor;
        public int overlayColor;

        public ItemTheme(int iconColor, int textColor, int overlayColor) {
            this.iconColor = iconColor;
            this.textColor = textColor;
            this.overlayColor = overlayColor;
        }

        public static class Builder {
            private Context context;
            int iconColor;
            int textColor;
            int overlayColor;

            public Builder(Context context) {
                this.context = context;
                iconColor = context.getColor(ResourceTable.Color_action_item_icon);
                textColor = context.getColor(ResourceTable.Color_action_item_text);
                overlayColor = context.getColor(ResourceTable.Color_action_item_overlay);

            }

            public ItemTheme build() {
                return new ItemTheme(iconColor, textColor, overlayColor);

            }


        }
    }

    public static class DefaultItemThemeApplier implements AdapterCommonsKt.ItemThemeApplier {
        private int dividerColor;
        private ItemTheme theme;

        public DefaultItemThemeApplier(int dividerColor, ItemTheme theme) {
            this.dividerColor = dividerColor;
            this.theme = theme;
        }

        @Override
        public void invoke(Object i, BaseRecyclerAdapter.ViewHolder viewHolder) {
            if (viewHolder instanceof DividerItem.ViewHolder) {
                DividerItem.ViewHolder viewHolder1 = (DividerItem.ViewHolder) viewHolder;
                viewHolder1.setColor(dividerColor);
            } else if (viewHolder instanceof SimpleActionItem.ViewHolder) {
                SimpleActionItem.ViewHolder viewHolder1 = (SimpleActionItem.ViewHolder) viewHolder;
                viewHolder1.setTheme(theme);
            }
        }
    }

}
