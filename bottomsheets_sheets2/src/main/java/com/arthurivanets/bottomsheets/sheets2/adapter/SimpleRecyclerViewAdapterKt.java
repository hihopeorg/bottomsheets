/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.sheets2.adapter;

import com.arthurivanets.adapter.recyclerview.BaseRecyclerAdapter;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import ohos.utils.zson.ZSONArray;
import ohos.utils.zson.ZSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SimpleRecyclerViewAdapterKt {

    private static class ViewType {
        int type;

        public ViewType(int type) {
            this.type = type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }
    }

    public static class SimpleRecyclerViewAdapter<IT extends AdapterCommonsKt.Item> extends BaseRecyclerAdapter<IT> {

        private LayoutScatter inflater;
        private ArrayList<AdapterCommonsKt.ViewHolderFactory> viewHolderFactories = new ArrayList();
        List<IT> items = new ArrayList();

        private AdapterCommonsKt.ItemThemeApplier itemThemer;
        public ListenerBinder listenerBinder;

        public interface ListenerBinder<IT> {
            void invoke(IT it, BaseRecyclerAdapter.ViewHolder viewHolder);
        }

        public void setItemThemer(AdapterCommonsKt.ItemThemeApplier itemThemer) {
            this.itemThemer = itemThemer;

        }

        public void setListenerBinder(ListenerBinder<IT> listenerBinder) {
            this.listenerBinder = listenerBinder;

        }

        public void setItems(List<IT> items) {
            this.items = items;
        }


        public SimpleRecyclerViewAdapter(Context context, List<IT> items) {
            super(context, items);
            inflater = LayoutScatter.getInstance(context);
            extractViewHolderFactories(items);
            this.items = items;

        }

        @Override
        public ViewHolder onCreateViewHolder(ComponentContainer parent, int viewType) {
            AdapterCommonsKt.ViewHolderFactory viewHolderFactory =  viewHolderFactories.get(  viewType);
            ViewHolder viewHolder = viewHolderFactory.create(inflater, parent);
            if (viewHolder != null) {
                return viewHolder;
            } else {
                throw new RuntimeException("The ViewHolder factory was not found.");
            }

        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            IT it = items.get(position);
            it.bind(holder);
            if (holder instanceof AdapterCommonsKt.HasListeners) {
                listenerBinder.invoke(it, holder);
            }

            itemThemer.invoke(it, holder);
        }

        @Override
        public Object getItem(int i) {
            return items.get(i);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public int getItemComponentType(int position) {
            return position;
        }

        private void extractViewHolderFactories(List<IT> list) {
            viewHolderFactories.clear();

            for (IT item : list) {//164317044//10497298//168781536//30410846//246681868
                if (!viewHolderFactories.contains(item)) {
                    viewHolderFactories.add(item);
                }

            }
        }
    }

}
