/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.sheets2.dsl;

import com.arthurivanets.adapter.model.Item;
import com.arthurivanets.adapter.recyclerview.BaseRecyclerAdapter;
import com.arthurivanets.bottomsheets.BottomSheet;
import com.arthurivanets.bottomsheets.sheets2.ActionPickerBottomSheet;
import com.arthurivanets.bottomsheets.sheets2.ActionsKt;
import com.arthurivanets.bottomsheets.sheets2.adapter.AdapterCommonsKt;
import com.arthurivanets.bottomsheets.sheets2.adapter.DividerItem;
import com.arthurivanets.bottomsheets.sheets2.kotlin.Unit;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.app.Context;

public class CustomActionPickerDslBuilder {

    public static BottomSheet customActionPickerBottomSheet(Fraction fraction, CustomActionPickerConfigBuilder builder) {
        return customActionPickerBottomSheet(fraction.getFractionAbility(), builder);
    }

    public static BottomSheet customActionPickerBottomSheet(Ability ability, CustomActionPickerConfigBuilder builder) {
//        BaseActionPickerDslBuilderKt.ActionPickerConfig build = new CustomActionPickerConfigBuilder(ability.getContext()).build();
        BaseActionPickerDslBuilderKt.ActionPickerConfig build = builder.build();
        ActionPickerBottomSheet actionPickerBottomSheet = new ActionPickerBottomSheet(ability, build);
        return actionPickerBottomSheet;
    }

    public static class CustomActionPickerConfigBuilder extends BaseActionPickerDslBuilderKt.AbstractActionPickerConfigBuilder {

        public CustomActionPickerConfigBuilder(Context context) {
            super(context);
        }

        AdapterCommonsKt.ItemThemeApplier itemThemeApplier = (i, viewHolder) -> {

        };

        public CustomActionPickerConfigBuilder section() {
            items.add(new DividerItem(new Unit()));
            return this;
        }

        public void item(AdapterCommonsKt.Item item) {
            items.add(item);

        }


        @Override
        protected AdapterCommonsKt.ItemThemeApplier createItemThemeApplier() {
            return new CustomItemThemeApplier(dividerColor, itemThemeApplier);
        }
    }


    private static class CustomItemThemeApplier implements AdapterCommonsKt.ItemThemeApplier {
        private int dividerColor;
        private AdapterCommonsKt.ItemThemeApplier itemThemeApplier;

        public CustomItemThemeApplier(int dividerColor, AdapterCommonsKt.ItemThemeApplier itemThemeApplier) {
            this.dividerColor = dividerColor;
            this.itemThemeApplier = itemThemeApplier;
        }

        @Override
        public void invoke(Object i, BaseRecyclerAdapter.ViewHolder viewHolder) {
            if (viewHolder instanceof DividerItem.ViewHolder) {
                DividerItem.ViewHolder viewHolder1 = (DividerItem.ViewHolder) viewHolder;
                viewHolder1.setColor(dividerColor);
            } else {
                itemThemeApplier.invoke(i, viewHolder);
            }
        }
    }
}
