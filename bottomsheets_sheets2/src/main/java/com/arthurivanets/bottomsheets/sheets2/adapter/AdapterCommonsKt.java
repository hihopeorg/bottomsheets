/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.sheets2.adapter;

import com.arthurivanets.adapter.recyclerview.BaseRecyclerAdapter;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

public class AdapterCommonsKt {

    public interface ItemThemeApplier<T> {
        void invoke(T i, BaseRecyclerAdapter.ViewHolder viewHolder);

    }

    public interface ViewHolderFactory {

        BaseRecyclerAdapter.ViewHolder create(LayoutScatter inflater, ComponentContainer parent);

    }


    public interface Bindable {

        void bind(BaseRecyclerAdapter.ViewHolder viewHolder);

    }

    /**
     * A marker interface that is used to mark the [RecyclerView.ViewHolder]s that support
     * the assignment of the listeners.
     */
    public interface HasListeners {

    }

    public abstract static class Item<Model> implements ViewHolderFactory, Bindable {
        public Model model;

        public Item(Model model) {
            this.model = model;
        }

    }


    public abstract static class AbstractItem<Mode extends Object, ViewHolder extends BaseRecyclerAdapter.ViewHolder> extends Item<Mode> {

        public AbstractItem(Mode mode) {
            super(mode);
        }

        @Override
        public void bind(BaseRecyclerAdapter.ViewHolder viewHolder) {
            performBinding((ViewHolder) viewHolder);
        }

        protected abstract void performBinding(ViewHolder viewHolder);
    }


}
