/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.sheets2.adapter;


import com.arthurivanets.adapter.annotation.ColorInt;
import com.arthurivanets.adapter.recyclerview.BaseRecyclerAdapter;
import com.arthurivanets.bottomsheets.sheets2.ResourceTable;
import com.arthurivanets.bottomsheets.sheets2.kotlin.Unit;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.ShapeElement;

public class DividerItem extends AdapterCommonsKt.AbstractItem<Unit, DividerItem.ViewHolder> {


    public DividerItem(Unit unit) {
        super(unit);
    }

    @Override
    public BaseRecyclerAdapter.ViewHolder create(LayoutScatter inflater, ComponentContainer parent) {
        return new ViewHolder(inflater.parse(ResourceTable.Layout_item_divider,parent,false));
    }

    @Override
    protected void performBinding(ViewHolder viewHolder) {

    }


   public  class ViewHolder extends BaseRecyclerAdapter.ViewHolder {

        public ViewHolder(Component itemView) {
            super(itemView);
        }

        public void setColor(@ColorInt int color) {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(new RgbColor(color));
            itemView.setBackground(shapeElement);
        }

    }
}
