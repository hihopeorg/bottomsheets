/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arthurivanets.bottomsheets.sheets2;

import com.arthurivanets.bottomsheets.BaseBottomSheet;
import com.arthurivanets.bottomsheets.BottomSheet;
import com.arthurivanets.bottomsheets.sheets2.adapter.AdapterCommonsKt;
import com.arthurivanets.bottomsheets.sheets2.adapter.SimpleRecyclerViewAdapterKt;
import com.arthurivanets.bottomsheets.sheets2.dsl.BaseActionPickerDslBuilderKt;
import com.arthurivanets.bottomsheets.sheets2.dsl.DslCommonKt;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import ohos.app.Context;

public  class ActionPickerBottomSheet extends BaseBottomSheet {
    private Context context;
    private BaseActionPickerDslBuilderKt.ActionPickerConfig pickerConfig;
    private Component view;

    /**
     * The main constructor used for the initialization of the {@link BottomSheet}.
     *
     * @param hostActivity the activity the content view of which is to be used as a holder of the bottom sheet
     * @param config
     */
    public ActionPickerBottomSheet(Ability hostActivity, BaseActionPickerDslBuilderKt.ActionPickerConfig config) {
        super(hostActivity, config);
        this.context = hostActivity;
        this.pickerConfig = config;
        initHeader();
        initRecyclerView();
    }

    private void initHeader() {
        DslCommonKt.AuxiliaryView headerView = pickerConfig.getHeaderView();
        if(headerView!=null){
            StackLayout headerContainer = (StackLayout) view.findComponentById(ResourceTable.Id_headerContainer);
            addComponent(headerView.getView(context, headerContainer), new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT));

        }

    }

    private void initRecyclerView() {
        ListContainer listContainer = (ListContainer) view.findComponentById(ResourceTable.Id_recyclerView);
        listContainer.setLayoutManager(new DirectionalLayoutManager());
        listContainer.setItemProvider(initItemAdapter());

    }

    @Override
    protected Component onCreateSheetContentView(Context context) {
        view  =LayoutScatter.getInstance(context).parse(ResourceTable.Layout_view_action_picker_bottom_sheet_content, this, false);
        return view;
    }

    private SimpleRecyclerViewAdapterKt.SimpleRecyclerViewAdapter<AdapterCommonsKt.Item> initItemAdapter() {
        SimpleRecyclerViewAdapterKt.SimpleRecyclerViewAdapter adapter = new SimpleRecyclerViewAdapterKt.SimpleRecyclerViewAdapter(context, pickerConfig.getItems());
        adapter.setItemThemer(pickerConfig.itemThemeApplier);
        adapter.setListenerBinder((o, viewHolder) -> {
            AdapterCommonsKt.Item item = (AdapterCommonsKt.Item) o;
            if (item.model instanceof ActionsKt.Action) {
                viewHolder.itemView.setClickedListener(component -> {
                    handleActionClick((ActionsKt.Action) item.model);


                });
            }


        });

        return adapter;

    }

    private void handleActionClick(ActionsKt.Action action) {
        if (pickerConfig.isDismissableOnItemClick) {
            dismiss();
        }
        pickerConfig.onItemClickListener.invoke(action);
    }


}
