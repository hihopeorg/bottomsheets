/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.sheets2.dsl;

import com.arthurivanets.adapter.annotation.ColorInt;
import com.arthurivanets.adapter.annotation.LayoutRes;
import com.arthurivanets.bottomsheets.config.BaseConfig;
import com.arthurivanets.bottomsheets.sheets2.ActionsKt;
import com.arthurivanets.bottomsheets.sheets2.ResourceTable;
import com.arthurivanets.bottomsheets.sheets2.adapter.AdapterCommonsKt;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.arthurivanets.bottomsheets.config.BaseConfig.DEFAULT_ANIMATION_DURATION;
import static com.arthurivanets.bottomsheets.config.BaseConfig.DEFAULT_DIM_AMOUNT;

public class BaseActionPickerDslBuilderKt {

    public static class ActionPickerConfig implements BaseConfig {

        private int _dimColor;
        private float _dimAmount;
        private float _topGapSize;
        private float _extraPaddingTop;
        private float _extraPaddingBottom;
        private float _maxSheetWidth;
        private int _sheetBackgroundColor;
        private float _sheetCornerRadius;
        private long _sheetAnimationDuration;
        private boolean _isDismissableOnTouchOutside;
        public boolean isDismissableOnItemClick;
        private DslCommonKt.AuxiliaryView headerView;
        int dividerColor;
        List<AdapterCommonsKt.Item> items;
        public AdapterCommonsKt.ItemThemeApplier itemThemeApplier;
        public ItemClickListener onItemClickListener;

        public ActionPickerConfig(int _dimColor, float _dimAmount, float _topGapSize, float _extraPaddingTop, float _extraPaddingBottom, float _maxSheetWidth, int _sheetBackgroundColor, float _sheetCornerRadius, long _sheetAnimationDuration, boolean _isDismissableOnTouchOutside, boolean isDismissableOnItemClick, DslCommonKt.AuxiliaryView headerView, int dividerColor, List<AdapterCommonsKt.Item> items, AdapterCommonsKt.ItemThemeApplier itemThemeApplier, ItemClickListener onItemClickListener) {
            this._dimColor = _dimColor;
            this._dimAmount = _dimAmount;
            this._topGapSize = _topGapSize;
            this._extraPaddingTop = _extraPaddingTop;
            this._extraPaddingBottom = _extraPaddingBottom;
            this._maxSheetWidth = _maxSheetWidth;
            this._sheetBackgroundColor = _sheetBackgroundColor;
            this._sheetCornerRadius = _sheetCornerRadius;
            this._sheetAnimationDuration = _sheetAnimationDuration;
            this._isDismissableOnTouchOutside = _isDismissableOnTouchOutside;
            this.isDismissableOnItemClick = isDismissableOnItemClick;
            this.headerView = headerView;
            this.dividerColor = dividerColor;
            this.items = items;
            this.itemThemeApplier = itemThemeApplier;
            this.onItemClickListener = onItemClickListener;
        }

        public void setOnItemClickListener(ItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }


        public void setHeaderView(DslCommonKt.AuxiliaryView headerView) {
            this.headerView = headerView;
        }

        public DslCommonKt.AuxiliaryView getHeaderView() {
            return headerView;
        }

        public List<AdapterCommonsKt.Item> getItems() {
            return items;
        }

        @Override
        public int getDimColor() {
            return _dimColor;
        }

        @Override
        public float getDimAmount() {
            return _dimAmount;
        }

        @Override
        public float getTopGapSize() {
            return _topGapSize;
        }

        @Override
        public float getExtraPaddingTop() {
            return _extraPaddingTop;
        }

        @Override
        public float getExtraPaddingBottom() {
            return _extraPaddingBottom;
        }

        @Override
        public float getMaxSheetWidth() {
            return _maxSheetWidth;
        }

        @Override
        public int getSheetBackgroundColor() {
            return _sheetBackgroundColor;
        }

        @Override
        public float getSheetCornerRadius() {
            return _sheetCornerRadius;
        }

        @Override
        public long getSheetAnimationDuration() {
            return _sheetAnimationDuration;
        }

        @Override
        public boolean isDismissableOnTouchOutside() {
            return _isDismissableOnTouchOutside;
        }


          public interface ItemClickListener {

            void invoke(ActionsKt.Action action);

        }

    }

    abstract static class AbstractActionPickerConfigBuilder {
        protected Context context;
        private DslCommonKt.AuxiliaryView headerView;
        protected ArrayList<AdapterCommonsKt.Item> items = new ArrayList<>();
        @ColorInt
        int dividerColor;
        @ColorInt
        int dimColor;

        @ColorInt
        int backgroundColor;


        float dimAmount = DEFAULT_DIM_AMOUNT;
        float topGapSize = 0f;
        float extraPaddingTop = 0f;
        float extraPaddingBottom = 0F;
        float maxWidth;
        float cornerRadius;
        long animationDuration = DEFAULT_ANIMATION_DURATION;

        boolean isDismissableOnTouchOutside = true;
        boolean isDismissableOnItemClick = true;
//        var onItemClickListener : ((Action) -> Unit)? = null

       public ActionPickerConfig.ItemClickListener onItemClickListener;
//        interface ItemClickListener {
//            void invoke(ActionsKt.Action action);
//
//        }


        public AbstractActionPickerConfigBuilder(Context context) {
            this.context = context;
            dividerColor = context.getColor(ResourceTable.Color_actions_divider_color);
            dimColor = context.getColor(ResourceTable.Color_bottom_sheet_dim_color);
            backgroundColor = context.getColor(ResourceTable.Color_bottom_sheet_background_color);
            try {
                maxWidth = context.getResourceManager().getElement(ResourceTable.Float_bottom_sheet_max_sheet_width).getFloat();
                cornerRadius = context.getResourceManager().getElement(ResourceTable.Float_bottom_sheet_sheet_corner_radius).getFloat();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            } catch (WrongTypeException e) {
                e.printStackTrace();
            }
        }

        public void headerView(@LayoutRes int resId) {
            headerView(resId, true);
        }

        public void headerView(@LayoutRes int resId, boolean isSticky) {
            headerView = new DslCommonKt.AuxiliaryView();
            headerView.setResourceId(resId);
            headerView.setSticky(isSticky);

        }

        public void headerView(Component view) {
            headerView(view, true);
        }

        public void headerView(Component view, boolean isSticky) {
            headerView = new DslCommonKt.AuxiliaryView();
            headerView.setView(view);
            headerView.setSticky(isSticky);
        }

        public ActionPickerConfig build() {
            return new ActionPickerConfig(
                    dimColor,
                    dimAmount,
                    topGapSize,
                    extraPaddingTop,
                    extraPaddingBottom,
                    maxWidth,
                    backgroundColor,
                    cornerRadius,
                    animationDuration,
                    isDismissableOnTouchOutside,
                    isDismissableOnItemClick,
                    headerView,
                    dividerColor,
                    items,
                    createItemThemeApplier(),
                    onItemClickListener

            );
        }

        protected abstract AdapterCommonsKt.ItemThemeApplier createItemThemeApplier();
    }


}
