/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.sheets2.extensions;

import com.arthurivanets.adapter.annotation.ColorRes;
import com.arthurivanets.adapter.annotation.DimenRes;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import ohos.global.resource.*;

import java.io.IOException;

public class ContextExtensionsKt {


    public static LayoutScatter layoutInflater(Context context) {
        return LayoutScatter.getInstance(context);
    }

    public static float getDimension(Context context, @DimenRes int id) {
        try {
            return context.getResourceManager().getElement(id).getFloat();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getCompatColor(Context context, @ColorRes int id) {
        return context.getColor(id);
    }

}
