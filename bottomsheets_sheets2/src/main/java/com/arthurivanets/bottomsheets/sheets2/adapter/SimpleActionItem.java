/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.sheets2.adapter;

import com.arthurivanets.adapter.recyclerview.BaseRecyclerAdapter;
import com.arthurivanets.bottomsheets.sheets2.ActionsKt;
import com.arthurivanets.bottomsheets.sheets2.ResourceTable;
import com.arthurivanets.bottomsheets.sheets2.dsl.ActionPickerDslBuilderKt;
import com.arthurivanets.bottomsheets.sheets2.extensions.TextViewExtensionsKt;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

import java.util.List;

public class SimpleActionItem extends AdapterCommonsKt.AbstractItem<ActionsKt.SimpleAction, SimpleActionItem.ViewHolder> {
    public SimpleActionItem(ActionsKt.SimpleAction action) {
        super(action);
    }

    @Override
    public BaseRecyclerAdapter.ViewHolder create(LayoutScatter inflater, ComponentContainer parent) {
        return new ViewHolder(
                (Text) inflater.parse(
                        ResourceTable.Layout_item_simple_action,
                        parent,
                        false
                )
        );
    }

    @Override
    protected void performBinding(ViewHolder viewHolder) {
        if (model.icon != null) {
            Element element = model.icon.getIcon(viewHolder.actionView.getContext());
            element.setBounds(0,0,80,80);
            viewHolder.actionView.setAroundElementsPadding(60);
            viewHolder.actionView.setAroundElements(element, null, null, null);
        }

        viewHolder.actionView.setText(String.valueOf(model.title.getText(viewHolder.actionView.getContext())));
    }

    public class ViewHolder extends BaseRecyclerAdapter.ViewHolder implements AdapterCommonsKt.HasListeners {
        Text actionView;

        public ViewHolder(Text itemView) {
            super(itemView);
            this.actionView = itemView;
        }

        public void setTheme(ActionPickerDslBuilderKt.ItemTheme theme) {
            actionView.setTextColor(new Color(theme.iconColor));
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(new RgbColor(theme.overlayColor));
            actionView.setBackground(shapeElement);


        }


    }
}
