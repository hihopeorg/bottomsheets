/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.sheets2.dsl;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.app.Context;


public class DslCommonKt {

    public static class Icon {
        private Element element;
        private int resourceId = 0;

        public Icon(Element element, int resourceId) {
            this.element = element;
            this.resourceId = resourceId;
        }

        public Element getIcon(Context context) {
            if (element != null) {
                return element;
            } else
            if (resourceId != 0) {
                 element = ElementScatter.getInstance(context).parse(resourceId);
                if (element != null) {
                    return element;
                } else {
                    throw new IllegalArgumentException("Not Drawable Resource found for ResourceId(" + resourceId + ")");
                }
            } else {
                throw new IllegalStateException("The Icon is not set.");
            }

        }

    }


    public static class Text {
        private CharSequence text;
        private int resourceId;
        private Object args;


        public CharSequence getText(Context context) {
            if (text != null) {
                return text;
            } else if (resourceId != 0) {
                return context.getString(resourceId);
            } else {
                return "";
            }
        }


        public void setResourceId(int resourceId) {
            this.resourceId = resourceId;
        }

        public int getResourceId() {
            return resourceId;
        }

        public void setText(CharSequence text) {
            this.text = text;
        }

        public void setArgs(Object args) {
            this.args = args;
        }

        public Object getArgs() {
            return args;
        }
    }


    public static class AuxiliaryView {

        private Component view;
        private int resourceId;
        private boolean isSticky = false;

        public Component getView(Context context, ComponentContainer parent) {
            if (view != null) {
                return view;
            } else if (resourceId != 0) {
                return LayoutScatter.getInstance(context).parse(resourceId, parent, false);
            } else {
                throw new IllegalStateException("The View has not been set.");
            }
        }

        public void setSticky(boolean sticky) {
            isSticky = sticky;
        }

        public void setResourceId(int resourceId) {
            this.resourceId = resourceId;
        }

        public int getResourceId() {
            return resourceId;
        }

        public boolean isSticky() {
            return isSticky;
        }

        public void setView(Component view) {
            this.view = view;
        }

        public Component getView() {
            return view;
        }
    }

}
