/*
 * Copyright 2019 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.sheets2;

import com.arthurivanets.bottomsheets.sheets2.dsl.DslCommonKt;

public class ActionsKt {

    public static class ActionId {
        public long id;

        public ActionId(long id) {
            this.id = id;
        }
    }

    public static class Action {
        public long id = 0;
    }


    public static class SimpleAction extends Action {
        public DslCommonKt.Icon icon;
        public DslCommonKt.Text title;
    }
}
