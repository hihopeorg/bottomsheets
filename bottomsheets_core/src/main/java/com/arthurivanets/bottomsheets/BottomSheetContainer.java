/*
 * Copyright 2017 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets;

import com.arthurivanets.bottomsheets.annotation.NonNull;
import com.arthurivanets.bottomsheets.config.BaseConfig;
import com.arthurivanets.bottomsheets.listeners.AnimatorListenerAdapter;
import com.arthurivanets.bottomsheets.util.BingBottomSheetContainer;
import com.arthurivanets.bottomsheets.util.Preconditions;
import com.arthurivanets.bottomsheets.util.Utils;
import ohos.aafwk.ability.Ability;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.LinearShader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.TouchEvent;
import org.jetbrains.annotations.Nullable;


/**
 * <br>
 * A base container which incorporates the main handling of the bottom sheet related logic.
 * <br>
 * <br>
 * Is responsible for the management of the bottom sheet related views, as well as the animations
 * and related stuff.
 * <br>
 */
abstract class BottomSheetContainer extends StackLayout implements BottomSheet, Component.TouchEventListener {

    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "BottomSheetContainer_TAG");
    protected final BaseConfig mConfig;

    private float mDimAmount;

    // Dimensions
    private float mSheetCornerRadius;
    private float mMaxSheetWidth;
    private float mStatusBarSize;
    private float mTopGapSize;

    private Point mDisplaySize;

    // Colors
    private int mDimColor;
    private int mSheetBackgroundColor;

    // Animation related
    private long mAnimationDuration;
    private AnimatorValue mAnimator;

    // UI elements
    private ComponentContainer mContentContainer;
    private StackLayout mBottomSheetView;

    // States
    private State mState;

    private boolean mIsDismissableOnTouchOutside;

    // Callbacks & Listeners
    private Runnable mViewManagementAction;
    private OnDismissListener mOnDismissListener;

    BottomSheetContainer(@NonNull Ability hostActivity, @NonNull BaseConfig config) {
        super(hostActivity);
        this.setTouchEventListener(this);
        mConfig = Preconditions.checkNonNull(config);


        init(hostActivity);
    }


    private void init(Ability hostActivity) {
        initContainer(hostActivity);
        initResources();
        initBottomSheet();
    }


    private void initContainer(Ability hostActivity) {


        mContentContainer = BingBottomSheetContainer.getInstance().getComponentContainer();


        // ensuring that the sheet is rendered atop all the other views within the same View Group
    }


    private void initResources() {
        mDimAmount = mConfig.getDimAmount();

        initDimensions();
        initColors();
        initAnimations();
        initStates();
    }


    private void initDimensions() {
        mSheetCornerRadius = mConfig.getSheetCornerRadius();
        mStatusBarSize = Utils.getStatusBarSize(getContext());
        mTopGapSize = mConfig.getTopGapSize();
        mMaxSheetWidth = mConfig.getMaxSheetWidth();
        mDisplaySize = new Point();
    }


    private void initColors() {
        mDimColor = mConfig.getDimColor();
        mSheetBackgroundColor = mConfig.getSheetBackgroundColor();
    }


    private void initAnimations() {
        mAnimationDuration = mConfig.getSheetAnimationDuration();
    }


    private void initStates() {
        mState = State.COLLAPSED;
        mIsDismissableOnTouchOutside = mConfig.isDismissableOnTouchOutside();
    }


    private void initBottomSheet() {
        removeFromContainer();
        mBottomSheetView = new StackLayout(getContext());
        mBottomSheetView.setLayoutConfig(generateDefaultLayoutParams());
        mBottomSheetView.setBackground(createBottomSheetBackgroundDrawable());
        mBottomSheetView.setPadding(
                mBottomSheetView.getPaddingLeft(),
                ((int) mConfig.getExtraPaddingTop()),
                mBottomSheetView.getPaddingRight(),
                ((int) mConfig.getExtraPaddingBottom())
        );

        HiLog.debug(LABEL, "mBottomSheetView.PaddingLeft()==" + mBottomSheetView.getPaddingLeft() + "----mConfig.getExtraPaddingTop()==" + mConfig.getExtraPaddingTop());
        // Creating the actual sheet content view
        final Component createdSheetView = Preconditions.checkNonNull(onCreateSheetContentView(getContext()));

        // adding the created views
        mBottomSheetView.addComponent(createdSheetView);
        addComponent(mBottomSheetView);
    }


    protected final ComponentContainer.LayoutConfig generateDefaultLayoutParams() {
        final ComponentContainer.LayoutConfig layoutParams = new ComponentContainer.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT
        );
        return layoutParams;
    }


    private void addToContainer() {
        mContentContainer.addComponent(
                this,
                new LayoutConfig(
                        LayoutConfig.MATCH_PARENT,
                        LayoutConfig.MATCH_PARENT
                )
        );
        float pos = 1f * AttrHelper.vp2px(mContentContainer.getResourceManager().getDeviceCapability().height, mContentContainer.getContext());
        mBottomSheetView.setContentPosition(0, pos);

    }




    private void removeFromContainer() {
        mContentContainer.removeComponent(this);
    }


    /**
     * <br>
     * Used to create the content view of the bottom sheet.
     * <br>
     * <br>
     * (Will be the main content shown to the user)
     * <br>
     *
     * @param context a valid context of the parent view
     * @return the view to be used as a custom content view of bottom sheet
     */
    @NonNull
    protected abstract Component onCreateSheetContentView(@NonNull Context context);


    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (mIsDismissableOnTouchOutside) {
            dismiss();
        }


        return false;
    }


    @Override
    public final void show() {
        show(true);
    }


    @Override
    public final void show(final boolean animate) {
        if (isAttachedToContainer()) {
            return;
        }

        cancelStateTransitionAnimation();
        addToContainer();
        postViewShowingAction(animate);
    }


    @Override
    public final void dismiss() {
        dismiss(true);
    }


    @Override
    public final void dismiss(boolean animate) {
        if (isAttachedToContainer() && (!animate || !State.COLLAPSING.equals(mState))) {
            cancelStateTransitionAnimation();
            reportOnDismiss();
            postViewDismissingAction(animate);
        }
    }


    private void postViewShowingAction(final boolean animate) {
        postPendingViewManagementAction(() -> expandSheet(animate));
    }


    private void expandSheet(final boolean animate) {
        if (animate) {
            if (!State.EXPANDED.equals(mState) && !State.EXPANDING.equals(mState)) {
                setUiState(State.COLLAPSED);
                animateStateTransition(State.EXPANDING);

            }
        } else {
            setUiState(mState = State.EXPANDED);
        }
    }


    private void postViewDismissingAction(final boolean animate) {
        postPendingViewManagementAction(() -> collapseSheet(animate));
    }


    private void collapseSheet(final boolean animate) {
        if (animate) {
            if (!State.COLLAPSED.equals(mState) && !State.COLLAPSING.equals(mState)) {
                animateStateTransition(State.COLLAPSING);
            }
        } else {
            removeFromContainer();
            setUiState(mState = State.COLLAPSED);
        }
    }


    private void postPendingViewManagementAction(Runnable action) {
        getContext().getMainTaskDispatcher().delayDispatch(mViewManagementAction = action, 100);
    }


    private boolean isAttachedToContainer() {
        final int containerChildCount = mContentContainer.getChildCount();

        for (int i = 0; i < containerChildCount; i++) {
            if (mContentContainer.getComponentAt(i) == this) {
                return true;
            }
        }

        return false;
    }


    private void animateStateTransition(final State state) {
        // cancelling any currently active transition animation
        cancelStateTransitionAnimation();
        // performing the precalculations

        final boolean isExpanding = State.EXPANDING.equals(state);
        System.err.println("------isExpanding---" + isExpanding);
        final float startY = 1f * AttrHelper.vp2px(mContentContainer.getResourceManager().getDeviceCapability().height, mContentContainer.getContext());
        final float endY = (mContentContainer.getHeight() - mBottomSheetView.getHeight());
        if (mAnimator == null) {
            mAnimator = new AnimatorValue();
            mAnimator.setDuration(300);
        }

        if (mAnimator.isRunning()) {
            return;
        }
        //循环次数

        mAnimator.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        mAnimator.setValueUpdateListener((animatorValue, value) -> {

            if (isExpanding) {
                final float newY = (startY + (value * (endY - startY)));
                System.err.println("------Animator---onStart==" + newY);
                mBottomSheetView.setContentPosition(0, newY);
                setBackgroundAlpha(value);
            } else {
                final float newY = (endY + (value * mBottomSheetView.getHeight()));
                mBottomSheetView.setContentPosition(0, newY);
            }

        });


        mAnimator.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                mState = (isExpanding ? State.EXPANDING : State.COLLAPSING);
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {
                mState = State.COLLAPSED;
                removeFromContainer();
            }

            @Override
            public void onEnd(Animator animator) {
                if (isExpanding) {
                    mState = State.EXPANDED;
                } else {
                    mState = State.COLLAPSED;
                    removeFromContainer();
                }

            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        mAnimator.start();
    }


    private void cancelStateTransitionAnimation() {
        if ((mAnimator != null) && mAnimator.isRunning()) {
            mAnimator.stop();
            mAnimator.cancel();
        }
    }


    private void reportOnDismiss() {
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(this);
        }
    }


    private Element createBottomSheetBackgroundDrawable() {


        ShapeElement shapeElement = new ShapeElement();

        shapeElement.setRgbColor(new RgbColor(mSheetBackgroundColor));

        shapeElement.setCornerRadiiArray(new float[]{
                mSheetCornerRadius,
                mSheetCornerRadius,
                mSheetCornerRadius,
                mSheetCornerRadius,
                0f,
                0f,
                0f,
                0f
        });
        return shapeElement;
    }


    private void setBackgroundAlpha(float alpha) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(00, 00, 00, (int) (255 * alpha * mDimAmount)));
        setBackground(shapeElement);
    }


    private void setUiState(State state) {
        setBottomSheetState(state);
        setBackgroundState(state);
    }


    private void setBottomSheetState(State state) {
        if (State.EXPANDED.equals(state)) {
            mBottomSheetView.setPivotY(getEstimatedHeight() - mBottomSheetView.getEstimatedHeight());
        } else if (State.COLLAPSED.equals(state)) {
            mBottomSheetView.setPivotY(getEstimatedHeight());
        }
    }


    private void setBackgroundState(State state) {
        if (State.EXPANDED.equals(state)) {
            setBackgroundAlpha(1f);
        } else if (State.COLLAPSED.equals(state)) {
            setBackgroundAlpha(0f);
        }
    }


    @NonNull
    @Override
    public final State getState() {
        return mState;
    }


    @Override
    public final void setOnDismissListener(@Nullable OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }


}