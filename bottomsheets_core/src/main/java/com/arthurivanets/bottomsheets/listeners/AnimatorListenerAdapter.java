/*
 * Copyright 2017 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.listeners;


import ohos.agp.animation.Animator;

/**
 * A compat version of the {@link Animator.StateChangedListener}.
 */
public abstract class AnimatorListenerAdapter implements Animator.StateChangedListener, Animator.LoopedListener {


    private boolean mIsCancelled;


    protected AnimatorListenerAdapter() {
        mIsCancelled = false;

    }


    @Override
    public void onStart(Animator animator) {
        mIsCancelled = false;
        onAnimationStarted(animator);
    }

    public void onAnimationStarted(Animator animation) {

    }


    @Override
    public void onEnd(Animator animator) {
        onAnimationEnded(animator);
    }


    public void onAnimationEnded(Animator animation) {

    }

    @Override
    public void onCancel(Animator animator) {
        mIsCancelled = true;

        onAnimationCancelled(animator);
    }


    public void onAnimationCancelled(Animator animation) {

    }


    @Override
    public void onRepeat(Animator animator) {
        mIsCancelled = false;
        onAnimationRepeated(animator);
    }

    public void onAnimationRepeated(Animator animation) {

    }


    public final boolean isCancelled() {
        return mIsCancelled;
    }


}
