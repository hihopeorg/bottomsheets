/*
 * Copyright 2017 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.util;


import com.arthurivanets.bottomsheets.annotation.NonNull;
import ohos.app.Context;
import ohos.global.resource.ResourceManager;

/**
 * A set of the general project utils.
 */
public final class Utils {

    /**
     * Retrieves the size (height) of the system status bar.
     *
     * @param context a valid context
     * @return the retrieved size (height) of the system status bar
     */
    public static int getStatusBarSize(@NonNull Context context) {
        Preconditions.nonNull(context);

        final ResourceManager resources = context.getResourceManager();

        return 100;
    }




    /**
     * Retrieves the size (height) of the system navigation bar.
     *
     * @param context a valid context
     * @return the retrieved size (height) of the system navigation bar
     */
    public static int getNavigationBarSize(@NonNull Context context) {
        Preconditions.nonNull(context);
        return 100;
    }




}
