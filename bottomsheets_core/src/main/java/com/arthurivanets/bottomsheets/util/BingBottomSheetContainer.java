/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arthurivanets.bottomsheets.util;

import ohos.agp.components.ComponentContainer;

public class BingBottomSheetContainer {

  private   ComponentContainer componentContainer;
    private static BingBottomSheetContainer instance;

    private BingBottomSheetContainer() {
    }

    public static synchronized BingBottomSheetContainer getInstance() {
        if (instance == null) {
            instance = new BingBottomSheetContainer();
        }
        return instance;
    }
    public  void initBingBottomSheetContainer(ComponentContainer componentContainer){
      this.componentContainer=componentContainer;

    }

    public ComponentContainer getComponentContainer() {
        return componentContainer;
    }
}


