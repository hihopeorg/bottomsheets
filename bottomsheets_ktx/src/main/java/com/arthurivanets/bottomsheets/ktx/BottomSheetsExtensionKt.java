/*
 * Copyright 2017 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arthurivanets.bottomsheets.ktx;

import com.arthurivanets.adapter.model.BaseItem;
import com.arthurivanets.bottomsheets.BottomSheet;
import com.arthurivanets.bottomsheets.sheets.ActionPickerBottomSheet;
import com.arthurivanets.bottomsheets.sheets.CustomActionPickerBottomSheet;
import com.arthurivanets.bottomsheets.sheets.adapters.actionpicker.ActionItem;
import com.arthurivanets.bottomsheets.sheets.adapters.actionpicker.BaseActionItem;
import com.arthurivanets.bottomsheets.sheets.config.ActionPickerConfig;
import com.arthurivanets.bottomsheets.sheets.listeners.OnItemSelectedListener;
import com.arthurivanets.bottomsheets.sheets.model.Option;
import ohos.aafwk.ability.Ability;

import java.util.ArrayList;
import java.util.List;

public class BottomSheetsExtensionKt {
    /**
     * Initializes and shows the [Option] Action Picker Bottom Sheet.
     */
    public static ActionPickerBottomSheet showActionPickerBottomSheet(Ability ability, List<Option> options, ActionPickerConfig config, OnItemSelectedListener<Option> onItemSelectedListener,
                                                               BottomSheet.OnDismissListener onDismissListener) {
        ArrayList<ActionItem> actionItems = new ArrayList();
        for (Option option : options) {
            actionItems.add(new ActionItem(option));
        }
        ActionPickerBottomSheet actionPickerBottomSheet = ActionPickerBottomSheet.init(ability, actionItems, config);

        actionPickerBottomSheet.setOnItemSelectedListener(item -> {


            onItemSelectedListener.onItemSelected(item.getItemModel());
        });
        actionPickerBottomSheet.setOnDismissListener(onDismissListener);
        actionPickerBottomSheet.show();

        return actionPickerBottomSheet;
    }


    /**
     * Initializes and shows the Custom Action Picker Bottom Sheet.
     * Your custom action items of type [T] will be utilized.
     *
     * @param items                  your custom bottom sheet action items
     * @param config                 bottom sheet configuration (optional)
     * @param onItemSelectedListener item selection listener
     * @param onDismissListener      bottom sheet dismissal listener
     */

    public static  <T extends BaseActionItem> CustomActionPickerBottomSheet<T> showCustomActionPickerBottomSheet(Ability ability, List<T> items,
                                                                                                         ActionPickerConfig config,
                                                                                                         OnItemSelectedListener<T> onItemSelectedListener,
                                                                                                         BottomSheet.OnDismissListener onDismissListener) {
        CustomActionPickerBottomSheet customActionPickerBottomSheet = CustomActionPickerBottomSheet.init(ability, items, config);
        customActionPickerBottomSheet.setOnItemSelectedListener(onItemSelectedListener);
        if (onDismissListener != null) {
            customActionPickerBottomSheet.setOnDismissListener(onDismissListener);
        }
        customActionPickerBottomSheet.show();
        return customActionPickerBottomSheet;
    }





}
