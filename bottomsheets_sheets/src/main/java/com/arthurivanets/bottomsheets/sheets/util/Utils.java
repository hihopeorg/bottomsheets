/*
 * Copyright 2017 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.sheets.util;


import com.arthurivanets.adapter.annotation.ColorInt;
import com.arthurivanets.adapter.annotation.NonNull;
import com.arthurivanets.adapter.annotation.Nullable;
import com.arthurivanets.bottomsheets.ResourceTable;
import com.arthurivanets.bottomsheets.util.Preconditions;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;


/**
 * General Module Utils.
 */
public final class Utils {


    /**
     * Disables all the {@link ListContainer} animations.
     */
    public static void disableAnimations(@NonNull ListContainer recyclerView) {
        Preconditions.nonNull(recyclerView);
    }


    /**
     * Sets the {@link Text}'s left {@link PixelMap}.
     */
    public static void setDrawableLeft(@NonNull Text textView, @Nullable Element element) {
        Preconditions.nonNull(textView);
        element.setBounds(0, 0, 80, 80);
        textView.setAroundElementsPadding(60);
        textView.setAroundElements(element, null, null, null);
    }


    /**
     * Creates a formatted html text from the specified raw text.
     *
     * @param rawText the text to be formatted
     * @return the formatted html text
     */
    @SuppressWarnings("NewApi")
    public static CharSequence fromHtml(CharSequence rawText) {
        if (!(rawText instanceof String)) {
            return rawText;
        }
        final String text = (String) rawText;
        return text;
    }


}