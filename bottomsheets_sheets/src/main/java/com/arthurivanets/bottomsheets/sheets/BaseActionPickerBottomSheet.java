/*
 * Copyright 2017 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.sheets;



import com.arthurivanets.adapter.annotation.CallSuper;
import com.arthurivanets.adapter.annotation.LayoutRes;
import com.arthurivanets.adapter.annotation.NonNull;
import com.arthurivanets.adapter.annotation.Nullable;
import com.arthurivanets.adapter.model.BaseItem;
import com.arthurivanets.bottomsheets.BaseBottomSheet;
import com.arthurivanets.bottomsheets.sheets.adapters.actionpicker.ActionPickerBottomSheetRecyclerViewAdapter;
import com.arthurivanets.bottomsheets.sheets.adapters.actionpicker.ActionPickerItemResources;
import com.arthurivanets.bottomsheets.sheets.adapters.actionpicker.BaseActionItem;
import com.arthurivanets.bottomsheets.sheets.config.ActionPickerConfig;
import com.arthurivanets.bottomsheets.sheets.listeners.OnItemSelectedListener;
import com.arthurivanets.bottomsheets.sheets.util.Utils;
import com.arthurivanets.bottomsheets.util.Preconditions;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextTool;
import ohos.app.Context;

import java.util.List;



import static com.arthurivanets.bottomsheets.sheets.util.Utils.disableAnimations;

/**
 * <br>
 *      A base class to be used for the concrete implementation of the Action Picker Bottom Sheets.
 * <br>
 *      Provides the general skeleton for the final {@link com.arthurivanets.bottomsheets.BottomSheet} implementation.
 * <br>
 */
abstract class BaseActionPickerBottomSheet<
        IT extends BaseActionItem,
        VH extends BaseItem.ViewHolder<?>,
        IR extends ActionPickerItemResources
        > extends BaseBottomSheet {


    protected final ActionPickerConfig mConfig;

    private List<IT> mItems;

    private IR mItemResources;

    private boolean mShouldDismissOnItemSelection;

    private OnItemSelectedListener<IT> mOnItemSelectedListener;


    protected BaseActionPickerBottomSheet(@NonNull Ability context,
                                          @NonNull List<IT> items,
                                          @NonNull IR itemResources,
                                          @NonNull ActionPickerConfig config) {
        super(context, config);

        mItems = Preconditions.checkNonNull(items);
        mConfig = Preconditions.checkNonNull(config);
        mItemResources = Preconditions.checkNonNull(itemResources);
        mShouldDismissOnItemSelection = true;

        onSheetContentViewCreated();
    }


    @NonNull
    @Override
    protected final Component onCreateSheetContentView(@NonNull Context context) {
        return LayoutScatter.getInstance(context).parse(
                getContentViewLayoutResourceId(),
                this,
                false
        );
    }


    @CallSuper
    protected void onSheetContentViewCreated() {
        initTitleView();
        initRecyclerView();
    }


    private void initTitleView() {
        final Text titleTv = (Text) findComponentById(ResourceTable.Id_title);
        titleTv.setTextColor(Color.BLACK);

        titleTv.setTextSize( 25, Text.TextSizeType.FP);
        titleTv.setText(String.valueOf(Utils.fromHtml(mConfig.getTitle())));
        titleTv.setVisibility(!TextTool.isNullOrEmpty(mConfig.getTitle()) ? Component.VISIBLE : Component.HIDE);
    }


    private void initRecyclerView() {
        final ListContainer recyclerView = (ListContainer) findComponentById(ResourceTable.Id_recycler_view);
        disableAnimations(recyclerView);
        recyclerView.setLayoutManager(initLayoutManager(getContext()));
        recyclerView.setItemProvider(initAdapter(getContext()));
    }


    private LayoutManager initLayoutManager(Context context) {
        return new DirectionalLayoutManager();
    }


    private ActionPickerBottomSheetRecyclerViewAdapter<IT, VH, IR> initAdapter(Context context) {
        final ActionPickerBottomSheetRecyclerViewAdapter<IT, VH, IR> adapter = new ActionPickerBottomSheetRecyclerViewAdapter<>(
                context,
                mItems,
                mItemResources
        );
        adapter.setOnItemClickListener((view, item, position) -> onActionItemClicked(item));

        return adapter;
    }


    private void onActionItemClicked(IT item) {
        if(mShouldDismissOnItemSelection) {
            dismiss();
        }

        reportItemSelected(item);
    }


    private void reportItemSelected(IT item) {
        if(mOnItemSelectedListener != null) {
            mOnItemSelectedListener.onItemSelected(item);
        }
    }


    /**
     * Used to retrieve the appropriate content view layout resource id to be inflated
     * and used by the final {@link com.arthurivanets.bottomsheets.BottomSheet}.
     *
     * @return a valid layout resource id of a content view.
     */
    @LayoutRes
    protected abstract int getContentViewLayoutResourceId();


    /**
     * Registers the action item selection listener, to be invoked when the selectable action item is clicked (selected).
     *
     * @param onItemSelectedListener the listener
     */
    public final void setOnItemSelectedListener(@Nullable OnItemSelectedListener<IT> onItemSelectedListener) {
        mOnItemSelectedListener = onItemSelectedListener;
    }


    /**
     * Specifies the exact behavior of the {@link com.arthurivanets.bottomsheets.BottomSheet} in cases
     * when the action item is selected.
     *
     * @param shouldDismissOnItemSelection whether to dismiss the {@link com.arthurivanets.bottomsheets.BottomSheet} when action item is selected or not
     */
    public final void setDismissOnItemSelection(boolean shouldDismissOnItemSelection) {
        mShouldDismissOnItemSelection = shouldDismissOnItemSelection;
    }


}