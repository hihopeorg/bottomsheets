/*
 * Copyright 2017 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arthurivanets.bottomsheets.sheets.adapters.actionpicker;


import com.arthurivanets.adapter.model.BaseItem;
import com.arthurivanets.bottomsheets.sheets.ResourceTable;
import com.arthurivanets.bottomsheets.sheets.model.Option;
import com.arthurivanets.bottomsheets.sheets.util.Utils;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;

import java.io.IOException;

public final class ActionItemViewHolder extends BaseItem.ViewHolder<Option> {


    final Text titleTv;




    public ActionItemViewHolder(Component itemView) {
        super(itemView);

        this.titleTv = (Text) itemView.findComponentById(ResourceTable.Id_title);
    }




    @Override
    public final void bindData(Option option) {
        super.bindData(option);

        if(option.isValidIconIdSet()) {

                Utils.setDrawableLeft(this.titleTv, ElementScatter.getInstance(this.titleTv.getContext()).parse(option.getIconId()));

        }

        // title-related
        this.titleTv.setTextColor(new Color( option.getTitleColor()));
        this.titleTv.setText(String.valueOf(Utils.fromHtml(option.getTitle())));
    }




}