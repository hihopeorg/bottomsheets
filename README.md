## **bottomsheets**
本项目是基于开源项目 bottomsheets 进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/arthur3486/bottomsheets ）追踪到原项目版本

#### 项目介绍

* 项目名称 ：bottomsheets是一个从屏幕底部边缘向上滑出的一个面板
* 所属系列：ohos的第三方组件适配移植
* 功能：底部弹框展示。
* 项目移植状态：完成
* 调用差异：无
* 项目作者和维护人：hihope
* 联系方式：hihope@hoperun.com
* 原项目Doc地址：https://github.com/arthur3486/bottomsheets
* 原项目基线版本：v1.2.0  shal:761fd76b1f0bcec119c34a7ad41468c847816522
* 编程语言：java
* 外部库依赖：无

#### 效果展示图
![](./screenshots/bottomsheets.gif)


#### 安装教程
方法(一)
1. 下载bottomsheets的har包。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```
方法(二)

1.在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2.在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.arthurivanets.demo:adapster_ktx:1.0.0'
    implementation 'com.arthurivanets.demo:adapter:1.0.0'
    implementation 'com.arthurivanets.demo:bottomsheets_core:1.0.0'
    implementation 'com.arthurivanets.demo:bottomsheets_ktx:1.0.0'
    implementation 'com.arthurivanets.demo:bottomsheets_sheets:1.0.0'
    implementation 'com.arthurivanets.demo:bottomsheets_sheets2:1.0.0'
}

```
#### 使用说明
(1) 设置布局 ability_main.xml
```
<?xml version="1.0" encoding="utf-8"?>
<DependentLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:orientation="vertical">

    <DirectionalLayout
        ohos:id="$+id:contentStackLayout"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:alignment="horizontal_center"
        ohos:top_margin="200vp">

        <Button
            ohos:id="$+id:customBottomSheetBtn"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:text="$string:button_title_custom_bottom_sheet"
            ohos:text_size="20vp"/>

        <Button
            ohos:id="$+id:confirmationBottomSheetBtn"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:text="$string:button_title_confirmation_bottom_sheet"
            ohos:text_size="20vp"
            ohos:top_margin="20vp"/>

        <Button
            ohos:id="$+id:actionsBottomSheetBtn"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:text="$string:button_title_actions_bottom_sheet"
            ohos:text_size="20vp"

            ohos:top_margin="20vp"/>

        <Button
            ohos:id="$+id:customActionsBottomSheetBtn"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:text="$string:button_title_custom_actions_bottom_sheet"
            ohos:text_size="20vp"
            ohos:top_margin="20vp"/>

        <Button
            ohos:id="$+id:actionsBottomSheet2Btn"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:text="$string:button_title_actions_bottom_sheet2"
            ohos:text_size="20vp"
            ohos:top_margin="20vp"/>

        <Button
            ohos:id="$+id:customActionsBottomSheet2Btn"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:text="$string:button_title_custom_actions_bottom_sheet2"
            ohos:text_size="20vp"
            ohos:top_margin="20vp"/>


    </DirectionalLayout>

    <DirectionalLayout
        ohos:id="$+id:content"
        ohos:height="match_parent"
        ohos:width="match_parent"/>


</DependentLayout>


```
(2)设置绑定加载布局

```
     ComponentContainer componentContainer = (ComponentContainer) findComponentById(ResourceTable.Id_content);
     BingBottomSheetContainer.getInstance().initBingBottomSheetContainer(componentContainer);

```

（3）调用示例

```
 private void initView() {
        findComponentById(ResourceTable.Id_customBottomSheetBtn).setClickedListener(component -> {
            if (bottomSheet == null || (bottomSheet != null && bottomSheet.getState().equals(BottomSheet.State.COLLAPSED))) {
                showCustomBottomSheet();
            }
        });

        findComponentById(ResourceTable.Id_confirmationBottomSheetBtn).setClickedListener(component -> {
            if (bottomSheet == null || (bottomSheet != null && bottomSheet.getState().equals(BottomSheet.State.COLLAPSED))) {
                showDeletionConfirmationBottomSheet();
            }
        });

        findComponentById(ResourceTable.Id_actionsBottomSheetBtn).setClickedListener(component -> {
            if (bottomSheet == null || (bottomSheet != null && bottomSheet.getState().equals(BottomSheet.State.COLLAPSED))) {
                showActionsBottomSheet();
            }
        });
        findComponentById(ResourceTable.Id_customActionsBottomSheetBtn).setClickedListener(component -> {
            if (bottomSheet == null || (bottomSheet != null && bottomSheet.getState().equals(BottomSheet.State.COLLAPSED))) {
                showPeopleBottomSheet();
            }
        });
        findComponentById(ResourceTable.Id_actionsBottomSheet2Btn).setClickedListener(component -> {
            if (bottomSheet == null || (bottomSheet != null && bottomSheet.getState().equals(BottomSheet.State.COLLAPSED))) {
                showActionsBottomSheet2();
            }
        });
        findComponentById(ResourceTable.Id_customActionsBottomSheet2Btn).setClickedListener(component -> {
            if (bottomSheet == null || (bottomSheet != null && bottomSheet.getState().equals(BottomSheet.State.COLLAPSED))) {
                showPeopleBottomSheet2();
            }
        });
    }


    private void showPeopleBottomSheet() {
        dismissBottomSheet();

        ArrayList<PersonItem> items = new ArrayList<>();
        for (Person person : PersonProvider.getPeople()) {
            items.add(new PersonItem(person));
        }
        com.arthurivanets.bottomsheets.sheets.config.Config.Builder builder = new com.arthurivanets.bottomsheets.sheets.config.
                Config.Builder(this);
        ActionPickerConfig actionPickerConfig = builder.build();
        bottomSheet = showCustomActionPickerBottomSheet(this.getAbility(), items, actionPickerConfig, new OnItemSelectedListener() {
            @Override
            public void onItemSelected(Object item) {
                if (item instanceof BaseActionItem) {
                    BaseActionItem baseActionItem = (BaseActionItem) item;
                    shortToast(baseActionItem.getItemModel().toString());
                }

            }
        }, null);
    }


    private void showPeopleBottomSheet2() {
        List<Person> personIterator = PersonProvider.getPeople();

        CustomActionPickerDslBuilder.CustomActionPickerConfigBuilder actionPickerConfigBuilder = new CustomActionPickerDslBuilder.CustomActionPickerConfigBuilder(this);
        for (Person person : personIterator) {
            PersonAction personAction = new PersonAction(person);
            PersonItem2 personItem2 = new PersonItem2(personAction);
            actionPickerConfigBuilder.item(personItem2);

        }

        actionPickerConfigBuilder.onItemClickListener = action -> {
            shortToast("Person Clicked: [id: " + action.id + "]");
        };

        bottomSheet = CustomActionPickerDslBuilder.customActionPickerBottomSheet(this.getAbility(), actionPickerConfigBuilder);
        bottomSheet.show();
    }


    private void showActionsBottomSheet2() {
        ActionPickerDslBuilderKt.ActionPickerConfigBuilder actionPickerConfigBuilder = new ActionPickerDslBuilderKt.ActionPickerConfigBuilder(this.getContext());
        actionPickerConfigBuilder.action(new ActionsKt.ActionId(1), ResourceTable.Graphic_ic_outline_local_printshop_24px, ResourceTable.String_action_print);
        actionPickerConfigBuilder.action(new ActionsKt.ActionId(2), ResourceTable.Graphic_ic_outline_save_24px, ResourceTable.String_action_save);
        actionPickerConfigBuilder.action(new ActionsKt.ActionId(3), ResourceTable.Graphic_ic_outline_offline_bolt_24px, ResourceTable.String_action_recharge);
        actionPickerConfigBuilder.action(new ActionsKt.ActionId(4), ResourceTable.Graphic_ic_outline_thumb_up_24px, ResourceTable.String_action_like);
        actionPickerConfigBuilder.action(new ActionsKt.ActionId(5), ResourceTable.Graphic_ic_outline_thumb_down_24px, ResourceTable.String_action_dislike);

        actionPickerConfigBuilder.onItemClickListener = action -> {
            shortToast("ActionID: " + action.id);
        };
        bottomSheet = ActionPickerDslBuilderKt.actionPickerBottomSheet(this.getAbility(), actionPickerConfigBuilder);
        bottomSheet.show();
    }

    private void showCustomBottomSheet() {
        dismissBottomSheet();
        bottomSheet = new SimpleCustomBottomSheet(this.getAbility(), new com.arthurivanets.bottomsheets.config.Config.Builder(this).build());
        bottomSheet.show();
    }


    private void showDeletionConfirmationBottomSheet() {
        dismissBottomSheet();
        String title = getString(ResourceTable.String_confirmation_title_item_deletion);
        List<Option> options = ConfirmationActionsProvider.getGeneralDeletionConfirmationActionOptions(this);

        ActionPickerConfig actionPickerConfig = ConfigExtensionsKt.actionPickerConfig(this, title);

        bottomSheet = showActionPickerBottomSheet(this.getAbility(), options, actionPickerConfig, new OnItemSelectedListener<Option>() {
            @Override
            public void onItemSelected(Option item) {
                shortToast(item.getTitle().toString());
            }
        }, null);

    }

    private void showActionsBottomSheet() {
        dismissBottomSheet();
        List<Option> options = ActionsProvider.getActionOptions(this.getContext());
        com.arthurivanets.bottomsheets.sheets.config.Config.Builder builder = new com.arthurivanets.bottomsheets.sheets.config.Config.Builder(this);
        ActionPickerConfig actionPickerConfig = builder.build();
        bottomSheet = showActionPickerBottomSheet(this.getAbility(), options, actionPickerConfig, new OnItemSelectedListener<Option>() {
            @Override
            public void onItemSelected(Option item) {
                shortToast(item.getTitle().toString());
            }
        }, null);

    }

    private void dismissBottomSheet() {
        dismissBottomSheet(true);
    }

    private void dismissBottomSheet(boolean animate) {
        if (bottomSheet != null) {
            bottomSheet.dismiss(animate);
        }
        bottomSheet = null;
    }

```
#### 版本迭代
v1.0.0
#### 支持功能
  * 支持底部弹框显示
#### 版权和许可信息
```
/*
 * Copyright 2017 Arthur Ivanets, arthur.ivanets.l@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
```











